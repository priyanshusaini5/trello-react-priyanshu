import React, { Component } from "react";
import { Spinner } from "@chakra-ui/react";
import axios from "axios";

import Card from "./Card";
import Form from "./Form";
import { addCard, getCards, removeCard } from "./api";


class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cards: [],
      hideDiv: false,
      inputValue: "",
      hasLoaded: false,
      errorMsg: "",
    };
  }

  openHideDiv = () => {
    this.setState({
      hideDiv: true,
    });
  };
  closeInputDiv = () => {
    this.setState({
      hideDiv: false,
    });
  };
  inputState = (event) => {
    this.setState({
      inputValue: event.target.value,
    });
  };

  addNewCard = async () => {
    // console.log(this.props.lists.id);
    // await axios
    //   .post(
    //     `https://api.trello.com/1/cards?idList=${this.props.lists.id}&key=5d9ad57830f391db674ed0a647a10379&token=ATTAbdcbf73d94d163763575634d49b756b88137bba5627c1491fcdec1b6e608afdbA8DEF31A&name=${this.state.inputValue}`
    //   )
      addCard(this.props.lists.id,this.state.inputValue)
      .then((response) => {
        this.setState({
          cards: [...this.state.cards, response.data],
          inputValue: "",
        });
      })
      .catch((error) => console.log(error));
  };

  deleteCard = (event, id) => {
    event.stopPropagation();

    // axios
    //   .delete(
    //     `https://api.trello.com/1/cards/${id}?key=5d9ad57830f391db674ed0a647a10379&token=ATTAbdcbf73d94d163763575634d49b756b88137bba5627c1491fcdec1b6e608afdbA8DEF31A`
    //   )
      removeCard(id)
      .then(() => {
        this.setState({
          cards: this.state.cards.filter((card) => card.id !== id),
        });
      })
      .catch((error) => console.log(error));
  };

  componentDidMount() {
    // axios
    //   .get(
    //     `https://api.trello.com/1/lists/${this.props.lists.id}/cards?key=5d9ad57830f391db674ed0a647a10379&token=ATTAbdcbf73d94d163763575634d49b756b88137bba5627c1491fcdec1b6e608afdbA8DEF31A`
    //   )
      getCards(this.props.lists.id)
      .then((response) =>
        this.setState({ cards: response.data, hasLoaded: true })
      )
      .catch((error) => {
        console.log(error);
        this.setState({ hasLoaded: true });
        this.setState({ errorMsg: "Error retrieving list" });
      });
  }

  render() {
    let id = this.props.lists.id;
    // console.log(id);
    let closeaddButton = this.state.hideDiv ? "none" : "block";
    let openHideDiv = this.state.hideDiv ? "block" : "none";
    let allCards = this.state.cards.map((card) => {
      return <Card key={card.id} cards={card} deleteCard={this.deleteCard} />;
    });
    return (
      <div className="listContainer">
        {this.state.hasLoaded ? null : (
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="md"
          />
        )}
        {this.state.errorMsg ? <strong>{this.state.errorMsg}</strong> : null}

        <div className="listHead">
          <h5 className="listName">{this.props.lists.name}</h5>
          <button
            onClick={() => this.props.deleteList(this.props.lists.id)}
            className="btn-default deleteButtonForList"
          >
            X
          </button>
        </div>
        <div className="cards">{allCards}</div>
        <div>
          <button
            onClick={this.openHideDiv}
            className="addCardButton btn btn-success"
            style={{
              display: closeaddButton,
              backgroundColor: "rgb(0, 0, 0, 0.2)",
              border: "none",
              color: "black",
              fontStyle: "bold",
            }}
          >
            Add New Card
          </button>
        </div>
        <Form
          style={{ display: openHideDiv }}
          closeInputDiv={this.closeInputDiv} //send function to form
          inputState={this.inputState}
          input={this.state.inputValue}
          addNewCard={this.addNewCard}
          buttonTitle="card"
        />
      </div>
    );
  }
}

export default List;
