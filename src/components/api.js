import axios from "axios";


axios.defaults.baseURL = 'https://api.trello.com/1/';
axios.defaults.params = {};
axios.defaults.params['key'] = '5d9ad57830f391db674ed0a647a10379'
axios.defaults.params['token'] = 'ATTAbdcbf73d94d163763575634d49b756b88137bba5627c1491fcdec1b6e608afdbA8DEF31A';


export function getBoards() {
    return axios
        .get(`members/priyanshusaini3/boards`
        )
}

export function createBoard(name){
    return axios.post(`boards/?name=${name}&prefs_background=purple`)
}

export function getLists(boardId) {
    return axios.get(`boards/${boardId}/lists`)
}

export function deleteList(listId){
    return axios.put(`lists/${listId}/closed?value=true`)
}

export function addList(name,boardId){
    return axios.post(`lists?name=${name}&idBoard=${boardId}`)
}

export function getCards(id){
    return axios.get(`lists/${id}/cards`)
}

export function addCard(listId,name){
    return axios.post(`cards?idList=${listId}&name=${name}`)
}

export function removeCard(id){
    return axios.delete(`cards/${id}`)
}
