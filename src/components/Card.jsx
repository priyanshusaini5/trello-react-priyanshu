import React from 'react';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";


const Card = props => {
    // console.log(props)
  return (
    <div className='cardDiv'>
      <h6 className='cardName'>{props.cards.name}</h6>
      <div>
        <button
          onClick={event => props.deleteCard(event, props.cards.id)}
          className='deleteButton btn btn-xsm'>
            <FontAwesomeIcon icon={faTrash} size="sm" />
        </button>
      </div>
    </div>
  );
};

export default Card;
