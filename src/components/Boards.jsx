import React, { Component } from "react";

import axios from "axios";
import Board from "./Board";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { Spinner } from "@chakra-ui/react";

import { getBoards, createBoard } from "./api";
// import { Button, ButtonGroup } from "@chakra-ui/react";

class Boards extends Component {
  constructor(props) {
    super(props);

    this.state = {
      boards: [],
      hasLoaded: false,
      errorMsg: "",
    };
  }
  handleCreateBoard = (event) => {
    event.preventDefault();
    const newBoardName = event.target[0].value;
    // axios
    //   .post(
    //     `https://api.trello.com/1/boards/?name=${newBoardName}&key=5d9ad57830f391db674ed0a647a10379&token=ATTAbdcbf73d94d163763575634d49b756b88137bba5627c1491fcdec1b6e608afdbA8DEF31A&prefs_background=purple`
    //   )
      createBoard(newBoardName)
      .then((response) => {
        this.setState({
          boards: [...this.state.boards, response.data],
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  componentDidMount() {
    // axios
    //   .get(
      
      //     // "https://api.trello.com/1/members/priyanshusaini3/boards?key=5d9ad57830f391db674ed0a647a10379&token=ATTAbdcbf73d94d163763575634d49b756b88137bba5627c1491fcdec1b6e608afdbA8DEF31A"
      //   )
      getBoards()
      .then((response) => {
        // console.log("done", response.data);
        this.setState({ boards: response.data, hasLoaded: true });
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ hasLoaded: true });
        this.setState({ errorMsg: "Error retrieving boards" });
      });
  }

  render() {
    // console.log(this.state)
    return (
      <div className="boardsView">
        {this.state.hasLoaded ? null : (
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        )}
        {this.state.errorMsg ? <strong>{this.state.errorMsg}</strong> : null}
        {this.state.boards.map((board) => {
          return <Board key={board.id} board={board} />;
        })}

        <Form className="createBoard" onSubmit={this.handleCreateBoard}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Add a Board</Form.Label>
            <Form.Control type="text" placeholder="Board Title" />
          </Form.Group>
          <Button variant="primary" type="submit">
            Create Board
          </Button>
        </Form>
        
      </div>
    );
  }
}

export default Boards;
