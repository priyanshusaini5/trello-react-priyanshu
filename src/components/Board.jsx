import { textDecoration } from "@chakra-ui/react";
import React from "react";

import { Link } from "react-router-dom";

const Board = (props) => {
  const styles = {
    width: "280px",
    height: "150px",
    backgroundColor: "rgb(214, 214, 219)",
    backgroundRepeat: "no-repeat",
    objectFit: "contain",
    backgroundPosition: "center",
    borderRadius: "10px",
    opacity: "0.9",
    padding: "5px",
    color: "black",
    fontWeight: "700",
    fontSize: "24px",
    textAlign: "left",
    cursor: "pointer",
    textDecoration:"none",
  };
  return (
    <React.Fragment>
      <Link to={`/board/${props.board.id}`}>
        <div className="board-card" style={styles}>
          {props.board.name}
        </div>
      </Link>
    </React.Fragment>
  );
};

export default Board;
