import { list } from "@chakra-ui/react";
import axios from "axios";
import React, { Component } from "react";
import List from "./List";
import Form from "./Form";
import { Spinner } from "@chakra-ui/react";
import { getLists, deleteList, addList } from "./api";

class Lists extends Component {
  constructor(props) {
    super(props);

    this.state = {
      lists: [],
      hideDiv: false,
      inputValue: "",
      open: false,
      card: {},
      hasLoaded: false,
      errorMsg: "",
    };
  }

  openHideDiv = () => {
    this.setState({
      hideDiv: true,
    });
  };
  closeInputDiv = () => {
    this.setState({
      hideDiv: false,
    });
  };
  inputState = (event) => {
    this.setState({
      inputValue: event.target.value,
    });
  };
  addNewList = () => {
    // axios
    // .post(
    //   `https://api.trello.com/1/lists?name=${this.state.inputValue}&idBoard=63cbd493cb5ff0036f389485&key=5d9ad57830f391db674ed0a647a10379&token=ATTAbdcbf73d94d163763575634d49b756b88137bba5627c1491fcdec1b6e608afdbA8DEF31A`
    //   )
      addList(this.state.inputValue,this.props.match.params.id)
      .then((response) => {
        this.setState({
          lists: [...this.state.lists, response.data],
          inputValue: "",
        });
      });
    };
    
    deleteList = (id) => {
      // axios
    //   .put(
      //     `https://api.trello.com/1/lists/${id}/closed?key=5d9ad57830f391db674ed0a647a10379&token=ATTAbdcbf73d94d163763575634d49b756b88137bba5627c1491fcdec1b6e608afdbA8DEF31A&value=true`
      //   )
      deleteList(id)
      .then((response) =>
      this.setState({
        lists: this.state.lists.filter((list) => list.id !== id),
      })
      )
      .catch((error) => {
        console.log(`error deleting ${id} ${error}`);
      });
    };
    
  componentDidMount() {
    // console.log("aitaaasd", this.props);
    
    // axios
    //   .get(

    //     `https://api.trello.com/1/boards/${this.props.match.params.id}/lists?key=5d9ad57830f391db674ed0a647a10379&token=ATTAbdcbf73d94d163763575634d49b756b88137bba5627c1491fcdec1b6e608afdbA8DEF31A`
    //   )

    getLists(this.props.match.params.id)
      .then((response) =>
        this.setState({ lists: response.data, hasLoaded: true })
      )
      .catch((error) => {
        console.log(error);
        this.setState({ hasLoaded: true });
        this.setState({ errorMsg: "Error retrieving lists" });
      });
  }
  render() {
    let closeaddButton = this.state.hideDiv ? "none" : "block";
    let openHideDiv = this.state.hideDiv ? "block" : "none";
    console.log('party mode',this.props);
    return (
      <div
        style={{
          display: "flex",
        }}
        className="allLists"
      >
        {this.state.hasLoaded ? null : (
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        )}
        {this.state.errorMsg ? <strong>{this.state.errorMsg}</strong> : null}

        {this.state.lists.length
          ? this.state.lists.map((list) => (
              <List
                key={list.id}
                lists={list}
                deleteList={this.deleteList}
                openModal={this.openModal}
              />
            ))
          : null}
        <button
          onClick={this.openHideDiv}
          className="newListButton"
          style={{ display: closeaddButton }}
        >
          Add List
        </button>

        <Form
          style={{ display: openHideDiv }}
          closeInputDiv={this.closeInputDiv}
          inputState={this.inputState}
          input={this.state.inputValue}
          addNewCard={this.addNewList}
          buttonTitle="list"
        />
      </div>
    );
  }
}

export default Lists;
