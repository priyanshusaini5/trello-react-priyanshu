import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrello } from "@fortawesome/free-brands-svg-icons";
import { Button, ButtonGroup } from "@chakra-ui/react";

import { Link } from "react-router-dom";

const Header = (props) => {
  // console.log(props);
  return (
    <>
      <Navbar bg="light" variant="light">
        <Container>
          <Nav className="me-auto">
            <FontAwesomeIcon icon={faTrello} size="4x" />
          </Nav>
          <Link to="/boards">
            <Button colorScheme="blue">Boards</Button>
          </Link>
        </Container>
      </Navbar>
    </>
  );
};

export default Header;
