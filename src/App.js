
import './App.css';

import 'bootstrap/dist/css/bootstrap.min.css';
import Boards from './components/Boards'
import Header from './components/Header'
import axios from 'axios';
import React, { Component } from 'react'
import Lists from './components/Lists';
import { extendTheme } from '@chakra-ui/react';


import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

export class App extends Component {


  render() {
    
    return (
      <Router>

        <div className="App" >
          <Header />

          <Switch>

            <Route exact path="/boards" component={Boards}>

            </Route>

            <Route path="/board/:id" component={Lists} />
          </Switch>

        </div>
      </Router >
    )
  }
}

export default App

